const { User } = require('../lib/user')
const assert = require('assert')

describe('User',()=>{
  it('pass crypto',async ()=>{
    let [a,b]= await Promise.all(['5555','5555'].map(User.cryptoPass)) 
    assert.equal(a,b)
  })
  it("create",async()=>{
    let cn = 'yyyy'
    let user = new User({
      dn: [
        `cn=${cn}`,
        `o=feverone`,
        `ou=developer`, `ou=test`,
      ],
      password: 'xxxx',
      attributes: {
        mail: `${cn}@fevergroup.com`,
      }
    })
    await user.create()
  })
  it('find',async()=>{
    let a = await User.find(['cn=shynome.zhang','o=feverone'],'feverone')
  })
  after(async ()=>{
    let client = await require('../lib/db').getClient()
    await client.close()
  })
})
