const { config } = require('../lib/config')
const assert = require('assert')

describe('config',()=>{
  let name = 'test_config_name'
  let value = '66666'
  it('set',async ()=>{
    await config.set(name,'44444')
    await config.set(name,'55555')
  })
  it('emitter', async()=>{
    let waitChange = new Promise((rl,rj)=>{
      setTimeout(()=>rj('wait change timeout'),2e3)
      config.emitter.once('set',()=>rl())
    })
    await config.set(name,value)
    await waitChange
  })
  it('get', async()=>{
    assert.equal(
      value,
      await config.get(name)
    )
  })
  it('remove',async()=>{
    await config.remove(name)
    assert.equal(
      null,
      await config.get(name)
    )
  })
  after(async ()=>{
    let client = await require('../lib/db').getClient()
    await client.close()
  })
})
