const { getCollection } = require('./db')
const { EventEmitter } = require('events')

class Config {
  constructor(name='config'){
    this.collection = getCollection(name,'name')
    this.emitter = new EventEmitter()
  }
  /**
   * @param {string} name 
   * @returns {Promise<any>}
   */
  async get(name){
    let collection = await this.collection
    let result = await collection.findOne({ name, })
    return result && result.value
  }
  async set(name,value){
    let collection = await this.collection
    let result, updated = true
    let lastValue = await this.get(name)
    if(lastValue === null){
      result = await collection.insertOne({ name, value })
    }else if(JSON.stringify(lastValue) !== JSON.stringify(value)){
      result = await collection.updateOne({ name, },{ $set: { value, } })
    }else{
      updated = false
    }
    updated && this.emitter.emit('set', name, value, lastValue)
    return result
  }
  async remove(name){
    let collection = await this.collection
    return await collection.deleteOne({ name, })
  }
}

exports.Config = Config
exports.config = new Config('config')