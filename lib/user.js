/// <reference path="./user.type.d.ts" />
const crypto = require('crypto')
const ldap = require('ldapjs')
const { config } = require('./config')
const { getCollection } = require('./db')

exports.BaseDNListConfigName = 'BaseDNList'

class User {
  /**
   * @param {UserType.Info} user 
   */
  constructor(user){
    /**@type {UserType.Info} */
    this.info = {
      dn:user.dn,
      cn: user.cn || user.dn.filter(v=>v.indexOf('cn=')===0)[0].slice(3),
      ...((typeof user.password === 'string' && !!user.password)?{password: User.cryptoPass(user.password)}:{}),
      attributes:{
        ...user.attributes,
        active: typeof user.attributes.active === 'boolean'?user.attributes.active:true,
      }
    }
    if(!this.info.cn){
      throw new Error('The cn field is reuqired')
    }
  }
  async create(){
    let companyList = User.getCompanyList(this.info.dn)
    await config.set(exports.BaseDNListConfigName,companyList)
    let collection = await User.getCollection()
    let resultList = await collection.insertOne(this.info).then(
      res=>({res}),
      err=>({err}),
    )
    return resultList
  }
  /**
   * @param {ldap.RDN[]} rdnList
   */
  static RDNListFormat(rdnList){
    return rdnList.map(({ attrs })=>{
      return Object.keys(attrs)
        .map(k=>attrs[k])
        .map(v=>v.name+'='+v.value)
    }).reduce((t,v)=>t.concat(v),[])
  }
  /**
   * @param {string[]} rdnList 
   */
  static getCompanyList(rdnList){
    return rdnList.filter(v=>v.indexOf('o=')===0).map(v=>v.slice(2))
  }
  /**
   * @param {string[]} filter 
   * @param {Object} [query] 
   * @returns {Promise<UserType.Info[]>}
   */
  static async find(filter,query={}){
    let collection = await User.getCollection()
    /**@type {UserType.Info[]} */
    let res = await collection.find({
      dn: { '$all': filter },
      'attributes.active': true,
      ...query
    }).toArray()
    // @ts-ignore
    return res.map(v=>({ ...v, password: null }))
  }
  /**
   * @param {string[]} filter 
   * @param {string} pass
   */
  static async auth(filter, pass){
    let users = await User.find(filter,{
      'password': User.cryptoPass(pass),
    })
    return users
  }
  static async getCollection(){ return getCollection('users','cn') }
  /**
   * @param {string} pass 
   * @returns {string}
   */
  static cryptoPass(pass){
    return crypto.pbkdf2Sync(pass,'utcvbgrtrhgbvsubi3totyr',4096,64,'sha512').toString('hex')
  }
}

exports.User = User
