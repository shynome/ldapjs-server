const pem = require('pem')
const fse = require('fs-extra')
const path = require('path')
const { dataDir } = require('./index')

const pemDir = path.join(dataDir,'pem')
const crtPath = path.join(pemDir,'ldaps.crt')
const keyPath = path.join(pemDir,'ldaps.key')

/**@return {Promise<{ certificate:string, serviceKey:string }>} */
exports.getKeys = async ()=>{
  await fse.mkdirp(pemDir)
  if(
    fse.existsSync(crtPath) &&
    fse.existsSync(keyPath)
  ){
    return {
      certificate: await fse.readFile(crtPath,'utf8'),
      serviceKey: await fse.readFile(keyPath,'utf8'),
    }
  }
  /**@type {pem.CertificateCreationResult} */
  let keys = await new Promise((rl,rj)=>{
    pem.createCertificate(
      {
        days: 36500,
        selfSigned: true,
      },
      async (err,keys)=>{
        err!==null?rj(err):rl(keys)
      }
    )
  })
  await Promise.all([
    fse.writeFile(crtPath,keys.certificate),
    fse.writeFile(keyPath,keys.serviceKey),
  ])
  return keys
}