const ldap = require('ldapjs')
require('./ldap.extend')
const logger = require('log4js').getLogger()
const { User } = require('./user') 

var _epxorts = {}

_epxorts.server = null
/**@return {Promise<ldap.Server>} */
_epxorts.getServer = async ()=>{
  if(_epxorts.server!==null){
    return _epxorts.server
  }
  return _epxorts.server = new Promise((rl,rj)=>{
    const server = ldap.createServer({ log: require('./ldap.logger') })
    rl(server)
  })
}

/**
 * @param {string} BaseDN
 */
_epxorts.addBaseDN = async (BaseDN)=>{

  logger.info(`add BaseDN:${BaseDN}`)
  
  const server = await _epxorts.getServer()
  let dn = 'o='+BaseDN
  
  server.bind(dn,  async (req,res,next)=>{
    let filter = User.RDNListFormat(req.dn.rdns)
    let { 0: user } = await User.auth(filter,req.credentials)
    if(typeof user === 'undefined'){
      return next(new ldap.InappropriateAuthenticationError())
    }
    // @ts-ignore
    req.connection.ldap.user = user
    res.end()
    return next()

  })
  
  server.search(dn, (req,res,next)=>{
    
    var isSearch = (req instanceof ldap.SearchRequest);
    switch(true){
      default:
        return next();
      case !req.connection.ldap.bindDN.childOf(dn):
      case !req.connection.ldap.bindDN.equals('ou=admin') && !isSearch:
        return next(new ldap.InsufficientAccessRightsError());
    }
    
  }, async (req,res,next)=>{
    
    let users = /**@type {UserType.Info[]} */([])
    
    if(req.connection.ldap.bindDN.equals('ou=admin')){
      let filter = User.RDNListFormat(req.connection.ldap.bindDN.rdns)
      users = await User.find(filter)
    }else{
      let { user } = /**@type {{user: UserType.Info}} */(/**@type {any} */(req.connection.ldap))
      if(typeof user === 'undefined'){
        return next(new ldap.InappropriateAuthenticationError())
      }
      users = [ user ]
    }
    
    let userList = users.map(user=>({
      dn: user.dn.join(','),
      objectclass: [],
      ...(user.dn.reduce((t,e)=>{
        let [k,...v] = e.split('=')
        if(typeof t[k] !== 'undefined'){
          if(!Array.isArray(t[k])){
            t[k] = [t[k]]
          }
          t[k] = t[k].concat(v.join('='))
        }else{
          t[k]=v.join('=')
        }
        return t
      },{})),
      attributes: user.attributes
    }))
    
    userList = userList.filter(user=>req.filter.matches(user))
    
    for(let user of userList){
      res.send(user)
    }

    res.end()
    return next()
  })

}

module.exports = _epxorts