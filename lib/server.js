const ldap = require('./ldap');
const { config } = require('./config')
const logger = require('log4js').getLogger()
const { BaseDNListConfigName } = require('./user')

async function main(){

  var server = await ldap.getServer()
  
  /**@type {string[]} */
  let BaseDNList = await config.get(BaseDNListConfigName)
  if(BaseDNList !== null){
    BaseDNList.forEach(BaseDN=>ldap.addBaseDN(BaseDN))
  }
  config.emitter.on('set',(...args)=>{
    console.log(args)
  })
  config.emitter.on('set',async (name, value)=>{
    debugger
    if(name !== BaseDNListConfigName)return;
    /**@type {string[]} */
    let newBaseDNList = value.filter(v=>!BaseDNList.includes(v))
    BaseDNList = BaseDNList.concat(newBaseDNList)
    newBaseDNList.forEach(BaseDN=>ldap.addBaseDN(BaseDN))
  })

  server.listen(1389, function() {
    logger.info('LDAP server listening at %s', server.url);
  });
  
}
main()