require('dotenv').config()
const path = require('path')
const { getEnv } = require('./tools')

require('log4js').configure({
  appenders: {
    console: { type: 'console' }
  },
  categories: {
    default: {
      level: getEnv('LOG_LEVEL') || 'info',
      appenders: ['console'],
    } 
  } 
})

exports.dataDir = path.join(process.cwd(),'data')

exports.config = {
  mongo_link:  getEnv('mongo_link', true),
  mongo_db:  getEnv('mongo_db', true),
}
