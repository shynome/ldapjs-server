export as namespace UserType
export interface Info {
  dn: string[],
  /**唯一用户名 */
  cn?: string
  /**密码 */
  password?: string,
  attributes: {
    active?: boolean
    [key:string]: any,
  }
}