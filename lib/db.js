const mongodb = require('mongodb')
const { config } = require('./index')
const logger = require('log4js').getLogger()

var _exports = {}

/**@type {Promise<mongodb.MongoClient>} */
_exports.client = null

_exports.getClient = ()=>{
  if(_exports.client !== null){
    return _exports.client
  }else{
    return _exports.client = mongodb.MongoClient.connect(config.mongo_link,{ useNewUrlParser: true })
    .then(
      client=>{
        logger.info(`mongo link successful. link url: ${config.mongo_link}`)
        return client
      },
      err=>{
        logger.info(`mongo link fail. link url: ${config.mongo_link}`)
        throw err
      }
    )
  }
}

_exports.getDb = async ()=>{
  let client = await _exports.getClient()
  return client.db(config.mongo_db)
}

_exports._collectionPromise = {}
/**
 * @param {string} name
 * @param {string} unique_index
 * @returns {Promise<mongodb.Collection>}
 */
_exports.getCollection = (name,unique_index)=>{
  if(typeof _exports._collectionPromise[name] !== 'undefined'){
    return _exports._collectionPromise[name]
  }
  return _exports._collectionPromise[name] = new Promise(async rl=>{
    let db = await  _exports.getDb()
    let collection = await db.createCollection(name)
    if(!(await collection.indexExists(unique_index))){
      collection.createIndex(unique_index,{ unique: true })
    }
    rl(collection)
  })
}

module.exports = _exports
