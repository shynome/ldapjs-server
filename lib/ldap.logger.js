// @ts-nocheck

const logger = require('log4js').getLogger()

const transfromLogger = (method='')=>{
  return (...args)=>{
    if(args.length===0){
      method[0] = method.toUpperCase()+method.slice(1)
      return logger[`is${method}Enabled`]
    }
    return logger[method](...args)
  }
}

module.exports = {
  debug: transfromLogger('debug'),
  trace: transfromLogger('trace'),
  warn:  transfromLogger('warn'),
  error: transfromLogger('error'),
}