// @ts-nocheck
/**
 * - 添加中间件 async 错误支持
 */

const Server = require('ldapjs').Server

function MiddlewareErrorHandler(nextHandler){
  return async function (req,res,next){
    return nextHandler.apply(this,arguments).catch(err=>next(err))
  }
} 

Server.prototype._origin_mount = Server.prototype._mount

Server.prototype._mount = function (op, name, argv, notDN){
  if(Array.isArray(argv)){
    argv = argv.map(f=>{
      if(typeof f !== 'function'){
        return f
      }
      if(Object.prototype.toString.call(f) !== '[object AsyncFunction]'){
        return f
      }
      return MiddlewareErrorHandler(f)
    })
  }
  return this._origin_mount(op, name, argv, notDN)
}
